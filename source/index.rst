.. sphinx_doc documentation master file, created by
   sphinx-quickstart on Sat Dec 21 23:17:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

基于stm32F407嵌入式底层驱动开发实践
======================================
.. toctree::
   :maxdepth: 1
   :caption: 关于本项目

   README
   
.. toctree::
   :maxdepth: 1
   :caption: 产品说明

   spec/硬件产品手册/产品手册（硬件）

.. toctree::
   :maxdepth: 1
   :caption: 准备课程
   :numbered:
   
   pre/1嵌入式入门/嵌入式入门
   pre/2软硬件环境准备/软硬件环境准备
   pre/3开发环境优化与技能准备/开发环境优化与技能准备
   pre/4基于标准库建立工程模板/基于标准库建立工程模板
   
.. toctree::
   :maxdepth: 1
   :caption: 实践教程
   :numbered:
   
   base/01IO输出/IO口输出-流水灯-证明程序在运行

.. toctree::
   :maxdepth: 1
   :caption: 提高教程
   :numbered:
   

.. toctree::
   :maxdepth: 1
   :caption: 版权

   LICENSE

